<?php

/*
 * Copyright 2012 by Prisacaru Anatolie (aka shark0der)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once 'fn.php';

function test($assertion, $message) {

    $result = assert($assertion) ? 'PASS' : 'FAIL';
    prettyprint(
            str_pad($message, 78)
            . $result
    );
}

test('read("Please type 1") == "1"', 'Read int from STDIN');
test('read("Please type \'hacking\'") == "hacking"', 'Read string from STDIN');

test(
        'preg_match("/^[\d]{1,3}.[\d]{1,3}.[\d]{1,3}.[\d]{1,3}$/", trim(post("http://icanhazip.com/")))'
        , 'post() function result return'
);

test(
        'preg_match("/key: \'x\' value: \'y\'/", trim(post("http://posttestserver.com/post.php?dump", array("x" => "y"))))'
        , 'post() function post body sending'
);
