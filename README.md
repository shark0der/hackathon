# Hack over the hackathon #

## Overview ##

This project is a security audit of the audit.hackathon.ro site (an early fork
of the main site www.hackathon.ro). All the following issues were already fixed
at the moment of this project presentation.

Below, I'll present the list of vulnerabilities I have found. The lists starts
with the less "innocent" and goes up to the most dangerous vulnerabilities. As
a reference, almost all of these are part of the [OWASP Top 10 Application
Security Risks of 2010](https://www.owasp.org/index.php/Top_10_2010-Main) list.

As a bonus, this project contains a POC (Hackover event password reset tool)
written in PHP that runs in command line.

## Vulnerabilities ##

1. **[Insufficient Transport Layer Protection - A9](https://www.owasp.org/index.php/Top_10_2010-A9)**

    The audited project does not have SSL at all, although SSL is at least
    recommended for login pages and all private pages and services. The data
    transmitted over the web are vulnerable to network traffic sniffing attacks.
    This is a common problem now-days. Keeping in mind that the server uses
    cPanel, it would be easy enough to add a SSL certificate and redirect all
    the HTTP requests to HTTPS using a .htaccess file.

2. **[Full Path Disclosure](https://www.owasp.org/index.php/Full_Path_Disclosure)**

    Full path disclosure allows attacker to know the absolute path of the
    current running application. By itself, this vulnerability is not harmful,
    but this can enable the exploiter to perform other attacks that require this
    information. The fix for this issue is to turn off PHP's errors, warnings
    other informational messages from being displayed using
    `error_reporting(0)` and `ini_set('display_errors', 0)`.

3. **[Cross-site request forgery - A5](https://www.owasp.org/index.php/Top_10_2010-A5)**

    The site lacks CSRF protection and this is a small gate that would allow a
    chain of other attacks. The simplest solution would be to plant a CSRF
    token in browser cookies and include a hidden input in every form. Checking
    the values of the twos on every POST request would stop Bob's malicious
    intentions.

4. **[Unvalidated Redirects and Forwards - A10](https://www.owasp.org/index.php/Top_10_2010-A10)**

    This is a security flaw that allows attacker to perform easy phishing
    attacks. The site trusts a few user inputs that allow the injection of own
    URLs to redirect the user after a specific action. The problem is that using
    previous vulnerability the attacker can perform a forged request and
    redirect the user to any site. A simple workaround would be to check if the
    user provided URLs are legit or not. The recommended workaround would be to
    store the redirect URLs only server side.

5. **[Broken Authentication and Session Management - A3](https://www.owasp.org/index.php/Top_10_2010-A3)**

    The authentication process does not generate a strong session identifier
    that would validate further user actions. This allows to update any user
    account and overwrite all his data by guessing a valid user id that matches
    his username. To solve this issue a strong session token should be
    implemented that would be stored for example in cookies, would have a
    limited validity time and would uniquely identify the session generated
    by user authentication.

6. **[Cross-Site Scripting - A2](https://www.owasp.org/index.php/Top_10_2010-A2)**

    The site allows through a very tricky way to inject JavaScript code into
    the user browser, via cookies. The project name is saved at some point in
    user cookies that are later used to prefill the project submission form.
    Using the previous CSRF flaw one could inject malicious code in the project
    name variable that will later be executed in the user's browser. Saving this
    type of data in cookies is generally not recommended and would definitely
    prevent this attack.

7. **[(MySQL) Injection - A1](https://www.owasp.org/index.php/Top_10_2010-A1)**

    This is categorized as the most popular type of vulnerability by OWASP and
    takes the head of the Top 10 Application Security Risks of 2010 (the list
    is revised every 3 years). There was a single vulnerable parameter.

8. **[Insecure Cryptographic Storage - A7](https://www.owasp.org/index.php/Top_10_2010-A7)**

    Usage of plain MD5 hash is an outdated method and highly discouraged as it
    is currently easy enough to be broken. Salting the password is good
    protection. Using time and resource consuming hashes with a tweakable
    complexity like [bcrypt](http://en.wikipedia.org/wiki/Bcrypt).
